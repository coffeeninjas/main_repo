package com.example.guaz.bquick.activities.Database.Model;

import android.support.annotation.NonNull;

import com.kontakt.sdk.android.common.Proximity;
import com.kontakt.sdk.android.common.profile.DeviceProfile;

import java.util.UUID;

/**
 * Created by Matma123 on 2017-10-08.
 */

public class Ibeacon implements Comparable<Ibeacon> {

    private int major;
    private int minor;
    private UUID proximityUUID;
    private String address;
    private int batteryPower;
    private double distance;
    private String firmwareVersion;
    private String name;
    private DeviceProfile deviceProfile;
    private Proximity proximity;
    private int rssi;
    private long timestamp;
    private int txPower;
    private String uniqueId;

    public Ibeacon(){

    }

    public Ibeacon(int minor, long timestamp, double distance) {
        this.minor = minor;
        this.timestamp = timestamp;
        this.distance = distance;
    }

    public int getMajor() {
        return major;
    }

    public String getName() {
        return name;
    }

    public DeviceProfile getDeviceProfile() {
        return deviceProfile;
    }

    public double getDistance() {
        return distance;
    }

    public int getBatteryPower() {
        return batteryPower;
    }

    public int getMinor() {
        return minor;
    }

    public int getRssi() {
        return rssi;
    }

    public int getTxPower() {
        return txPower;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Proximity getProximity() {
        return proximity;
    }

    public String getAddress() {
        return address;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public UUID getProximityUUID() {
        return proximityUUID;
    }

    @Override
    public int compareTo(@NonNull Ibeacon o) {
        int compareTimestamp = (int) ((Ibeacon) o).getTimestamp();
        return (int) (this.timestamp) - compareTimestamp;
    }
}
