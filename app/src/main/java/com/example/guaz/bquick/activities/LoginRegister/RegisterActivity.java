package com.example.guaz.bquick.activities.LoginRegister;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.guaz.bquick.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    public static final String TAG = "tomek";

    @BindView(R.id.emailRegisterInput)
    EditText emailRegisterInput;
    @BindView(R.id.passRegisterInput)
    EditText passRegisterInput;

    private FirebaseAuth firebaseAuth;

    @OnClick(R.id.createAccountBtn)
    public void createAccount() {
        if (passRegisterInput.getText().toString().length() > 6) {
            firebaseAuth.createUserWithEmailAndPassword(emailRegisterInput.getText().toString(), passRegisterInput.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d(TAG, "User Created!");
                        }
                    });
            emailRegisterInput.setText("");
            passRegisterInput.setText("");
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Password too short.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        firebaseAuth = FirebaseAuth.getInstance();
    }
}
