package com.example.guaz.bquick.activities.Database.Model;

/**
 * Created by Matma123 on 2017-10-27.
 */

public class Rank {

    private String userName;
    private String userTime;

    public Rank(String userName, String userTime) {
        this.userName = userName;
        this.userTime = userTime;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserTime() {
        return userTime;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserTime(String userTime) {
        this.userTime = userTime;
    }
}
