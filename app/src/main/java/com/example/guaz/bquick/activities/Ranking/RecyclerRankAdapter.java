package com.example.guaz.bquick.activities.Ranking;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.guaz.bquick.R;
import com.example.guaz.bquick.activities.Database.Model.Ibeacon;
import com.example.guaz.bquick.activities.Database.Model.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Matma123 on 2017-10-27.
 */

public class RecyclerRankAdapter extends RecyclerView.Adapter<RecyclerRankAdapter.ViewHolder> {

    List<User> userList = new ArrayList<>();

    Context context;
    User user = new User();


    public RecyclerRankAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.ranking_recycler_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String minor = "";
        String timestamp = "";
        User userData = userList.get(position);

        holder.nameText.setText(userData.getEmail());
        for (Ibeacon ib : userData.getIbeaconList()) {
            minor = minor + ib.getMinor();
            timestamp = timestamp + ib.getTimestamp() + "\n";
        }

        holder.bestTime.setText(userData.getBestTime() + " sec");
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CardView background;
        public TextView nameText, bestTime;

        public ViewHolder(View root) {
            super(root);

            background = (CardView) root.findViewById(R.id.card_background);
            bestTime = (TextView) root.findViewById(R.id.rankBestTime);
            nameText = (TextView) root.findViewById(R.id.rankNameText);
        }
    }
}