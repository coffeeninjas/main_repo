package com.example.guaz.bquick.activities.Ranking;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.guaz.bquick.R;
import com.example.guaz.bquick.activities.Database.Model.Ibeacon;
import com.example.guaz.bquick.activities.Database.Model.Track;
import com.example.guaz.bquick.activities.Database.Model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Guaz on 2017-10-07.
 */

public class DataActivity extends AppCompatActivity {
    @BindView(R.id.recyclerProgressBar)
    ProgressBar progressBar;
    @BindView(R.id.rankRecyclerView)
    RecyclerView rankRecyclerView;
    RecyclerRankAdapter adapter;
    List<User> userList = new ArrayList<>();
    List<Long> timeList;
    List<Track> trackList;
    String trackIDInput;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        ButterKnife.bind(this);
        reference = FirebaseDatabase.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        Intent i = getIntent();
        String trackId = i.getStringExtra("trackId");

        reference.child(trackId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    Track track = dataSnapshot1.getValue(Track.class);
                    trackList = new ArrayList<>();
                    trackList.add(track);

                    Log.d("trasa", " track size: " + trackList.get(0).getName());

                    User u = trackList.get(0).getUser();
                    Log.d("user", " " + u.getEmail());

                    timeList = new ArrayList<>();

                    for (Ibeacon b : u.getIbeaconList()) {
                        Log.d("tomek", "timestamp: " + b.getTimestamp() + " minor: " + b.getMinor() + " key " + dataSnapshot1.getKey());
                        timeList.add(b.getTimestamp());
                    }

                    Collections.sort(timeList);
                    float timeFloat = (timeList.get(timeList.size() - 1) - timeList.get(0));
                    u.setBestTime("" + timeFloat / 1000);

                    Collections.sort(timeList);
                    Collections.sort(userList);
                    userList.add(u);


                }
                adapter = new RecyclerRankAdapter(getApplicationContext());
                Collections.reverse(userList);
                adapter.setData(userList);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                rankRecyclerView.setLayoutManager(layoutManager);
                rankRecyclerView.setItemAnimator(new DefaultItemAnimator());
                rankRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        reference.child(userMail).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot data : dataSnapshot.getChildren()) {
//
//                    timeList = new ArrayList<>();
//                    User user = data.getValue(User.class);
//                    for (Ibeacon b : user.getIbeaconList()) {
//                        Log.d("tomek", "timestamp: " + b.getTimestamp() + " minor: " + b.getMinor() + " key " + data.getKey());
//                        timeList.add(b.getTimestamp());
//                    }
//
//                    Collections.sort(timeList);
//                    float timeFloat = (timeList.get(timeList.size() - 1) - timeList.get(0));
//                    user.setBestTime("" + timeFloat / 1000);
//                    Collections.sort(userList);
//
//                    userList.add(user);
//
//                }
//
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
    }



}