package com.example.guaz.bquick.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.example.guaz.bquick.R;
import com.example.guaz.bquick.activities.Database.Model.Ibeacon;
import com.example.guaz.bquick.activities.Database.Model.Track;
import com.example.guaz.bquick.activities.Database.Model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.kontakt.sdk.android.ble.configuration.ScanMode;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Guaz on 2017-10-07.
 */

public class MeasureActivity extends AppCompatActivity {

    private static final String APIE_KEY = "TlFElBMUqqQSnfmWYgRPZYjVpAwazqaf";


    private ProximityManager proximityManager;

    OrmLiteSqliteOpenHelper ormLiteSqliteOpenHelper;
    List<Long> list;
    List<Ibeacon> ibeaconList = new ArrayList<>();

    private DatabaseReference reference;
    private FirebaseAuth firebaseAuth;
    Ibeacon ibeacon;
    User u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measure);
        KontaktSDK.initialize(APIE_KEY);
        ButterKnife.bind(this);

        reference = FirebaseDatabase.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        Intent i = getIntent();
        proximityManager = ProximityManagerFactory.create(this);
        proximityManager.configuration()
                .scanMode(ScanMode.BALANCED)
                .scanPeriod(ScanPeriod.RANGING)
                .deviceUpdateCallbackInterval(TimeUnit.MILLISECONDS.toMillis(500));

        proximityManager.setIBeaconListener(new SimpleIBeaconListener() {
            @Override
            public void onIBeaconDiscovered(IBeaconDevice iBeacon, IBeaconRegion region) {
                if (iBeacon.getMinor() != 100 && iBeacon.getMinor() < 400) {
                    Log.d("tomek", String.valueOf(iBeacon.getDistance()));

                    ibeacon = new Ibeacon(iBeacon.getMinor(), iBeacon.getTimestamp(), iBeacon.getDistance());
                    ibeaconList.add(ibeacon);
                    String userEmail = firebaseAuth.getCurrentUser().getEmail().replace(".", "");
                    Log.d("tomek", String.valueOf(iBeacon.getMinor()));
                    Collections.sort(ibeaconList);
                    u = new User(userEmail, "wafel", ibeaconList, "");
                    Track t = new Track("trasa numer 1", getIntent().getStringExtra("trackId"), u);
                    if (ibeaconList.size() == 3) {
                        reference.child(String.valueOf(t.getTrackID())).push().setValue(t);
                        proximityManager.stopScanning();
                        finish();
                    }
                }
            }


            @Override
            public void onIBeaconLost(IBeaconDevice ibeacon, IBeaconRegion region) {
                super.onIBeaconLost(ibeacon, region);

                Log.d("tomek", "Ibeacon lost");
            }

            @Override
            public void onIBeaconsUpdated(List<IBeaconDevice> ibeacons, IBeaconRegion region) {
                super.onIBeaconsUpdated(ibeacons, region);
                Log.d("tomek", "size onUpdate  :" + ibeacons.size());
                Log.d("tomek", "size list:" + ibeaconList.size());

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
                Log.d("tomek", "scan started");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();


            }

    @Override
    protected void onStop() {
        // proximityManager.stopScanning();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        proximityManager.disconnect();
        proximityManager = null;
        super.onDestroy();
    }

    public String convertTimestamp(long timestamp) {
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date d = new Date((timestamp * 1000));
            return df.format(d);
        } catch (Exception e) {
            return "wrong Format";
        }
    }

}
