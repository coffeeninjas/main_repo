package com.example.guaz.bquick.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.example.guaz.bquick.R;
import com.example.guaz.bquick.activities.Ranking.DataActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogActivity extends AppCompatActivity {

    @BindView(R.id.enter_track_id)
    EditText enter_track_id;
    int activityId;

    @OnClick(R.id.trackIdBtn)
    public void settrackId() {
        String trackId = enter_track_id.getText().toString();
        Intent i = new Intent();
        if (activityId == 1) {
            i = new Intent(DialogActivity.this, MeasureActivity.class);
        } else if (activityId == 2) {
            i = new Intent(DialogActivity.this, DataActivity.class);
        }

        i.putExtra("trackId", trackId);
        startActivity(i);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        activityId = intent.getIntExtra("next_activity", 0);
    }
}
