package com.example.guaz.bquick.activities.Database.Model;

import android.support.annotation.NonNull;

import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Matma123 on 2017-10-27.
 */
public class User implements Comparable<User> {

    String email;
    String nickname;
    List<Ibeacon> ibeaconList;
    String bestTime;

    public User(String email, String nickname, List<Ibeacon> ibeaconList, String bestTime) {
        this.email = email;
        this.nickname = nickname;
        this.ibeaconList = ibeaconList;
        this.bestTime = bestTime;
    }

    public User() {
    }

    public List<Ibeacon> getIbeaconList() {
        return ibeaconList;
    }

    public String getEmail() {
        return email;
    }

    public String getNickname() {
        return nickname;
    }

    public static String getDate(long timestamp) {
        try {
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date netDate = (new Date(timestamp * 1000));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }

    public void setBestTime(String bestTime) {
        this.bestTime = bestTime;
    }

    public String getBestTime() {
        return bestTime;
    }

    @Override
    public int compareTo(@NonNull User o) {
        double compareBestTime = Double.valueOf(((User) o).getBestTime());
        double result = Double.valueOf(this.bestTime) - compareBestTime;

        return (int) result;
    }
}