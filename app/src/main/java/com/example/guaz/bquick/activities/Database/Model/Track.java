package com.example.guaz.bquick.activities.Database.Model;

import java.util.List;

/**
 * Created by Guaz on 2017-10-29.
 */

public class Track {
    private String name;
    private String trackID;
    private User user;

    public Track() {
    }

    public Track(String name, String trackID, User user) {
        this.name = name;
        this.trackID = trackID;
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public String getTrackID() {
        return trackID;
    }

    public User getUser() {
        return user;
    }
}
